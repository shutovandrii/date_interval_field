<?php

/**
 * @file
 * Contains \Drupal\date_interval_field\Plugin\Field\FieldFormatter\TimeSlotFieldFormatter.
 */

namespace Drupal\date_interval_field\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DateHelper;

/**
 * Plugin implementation of the 'time_slot_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "time_slot_field_formatter",
 *   label = @Translation("Time slot field formatter"),
 *   field_types = {
 *     "time_slot"
 *   }
 * )
 */
class TimeSlotFieldFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'day_format' => 0,
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['day_format'] = array(
      '#type' => 'select',
      '#title' => t('Select day format for timeslot'),
      '#default_value' => $this->getSetting('day_format'),
      '#options' => array('day of month', 'day of week'),
      '#description' => t('Widget changes for already used field will delete all content.'),
    );

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $options = array('day of month', 'day of week');
    $summary[] = t('Day format: @format', array('@format' => $options[$this->getSetting('day_format')]));

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $items = $items->getValue();
    $item_list = array();
    foreach ($items as $delta => $item) {
      $item_list[$delta] = $this->viewValue($item);
    }

    $header['day']['data'] = $this->getSetting('day_format') == 0 ? t('Day of the week') : t('Day of the month');
    $header['start_time']['data'] = t('Start Time');
    $header['end_time']['data'] = t('End Time');

    $output = array(
      '#type' => 'table',
      '#header' => $header,
      '#title' => '',
      '#rows' => $item_list,
      '#empty' => t("Disponibilities not set."),
      '#attributes' => [
        'class' => ['user-disponobilities-list-table']
      ]
    );
    $elements[0] = $output;
    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   */
  private function viewValue($item) {
    if ($this->getSetting('day_format') == 0) {
      $date_helper = new DateHelper();
      $days = $date_helper->weekDays(TRUE);
      $days = $date_helper->weekDaysOrdered($days);
      $day = $item['day'] < 7 ? $item['day'] : 0;
      $item_list['day']['data'] = $days[$day];
    }
    else {
      $item_list['day']['data'] = $item['day'];
    }
    $item_list['start_time']['data'] = date('H:i', strtotime($item['start_time']));
    $item_list['end_time']['data'] = date('H:i', strtotime($item['end_time']));

    return $item_list;
  }

}
