<?php

/**
 * @file
 * Contains \Drupal\date_interval_field\Plugin\Field\FieldType\TimeSlotFieldType.
 */

namespace Drupal\date_interval_field\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'time_slot_field_type' field type.
 *
 * @FieldType(
 *   id = "time_slot",
 *   label = @Translation("Time slot"),
 *   description = @Translation("Time slot store start and end time during the day"),
 *   default_widget = "time_slot",
 *   default_formatter = "time_slot_field_formatter",
 * )
 */
class TimeSlotFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return array(
      'datetime_type' => 'datetime',
    ) + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['day'] = DataDefinition::create('string')
      ->setLabel(t('Day of time slot'));

    $properties['start_time'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(t('Start time of the slot'))
      ->setRequired(TRUE);

    $properties['start_date'] = DataDefinition::create('any')
      ->setLabel(t('Computed date'))
      ->setDescription(t('The computed DateTime object.'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\datetime\DateTimeComputed')
      ->setSetting('date source', 'start_time');

    $properties['end_time'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(t('End time of the slot'))
      ->setRequired(TRUE);

    $properties['end_date'] = DataDefinition::create('any')
      ->setLabel(t('Computed date'))
      ->setDescription(t('The computed DateTime object.'))
      ->setComputed(TRUE)
      ->setClass('\Drupal\datetime\DateTimeComputed')
      ->setSetting('date source', 'end_time');

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = array(
      'columns' => array(
        'day' => array(
          'description'   => 'Timeslot day',
          'type'          => 'varchar',
          'size'          => 'normal',
          'length'        => 20,
          'not null'      => TRUE,
          'default'       => 'M',
        ),
        'start_time' => array(
          'description' => 'The start time of the slot.',
          'type' => 'varchar',
          'length' => 20,
        ),
        'end_time' => array(
          'description' => 'The end time of the slot.',
          'type' => 'varchar',
          'length' => 20,
        ),
      ),
    );

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('day')->getValue();
    return $value === NULL || $value === '';
  }

}
