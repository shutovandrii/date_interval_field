<?php

/**
 * @file
 * Contains \Drupal\date_interval_field\Plugin\Field\FieldWidget\TimeSlotFieldWidget.
 */

namespace Drupal\date_interval_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DateHelper;
use Drupal\Core\Datetime\DrupalDateTime;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Plugin implementation of the 'time_slot' widget.
 *
 * @FieldWidget(
 *   id = "time_slot",
 *   label = @Translation("Time slot"),
 *   field_types = {
 *     "time_slot"
 *   }
 * )
 */
class TimeSlotFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'day_format' => 1,
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['day_format'] = array(
      '#type' => 'select',
      '#title' => t('Select day format for timeslot'),
      '#default_value' => $this->getSetting('day_format'),
      '#options' => array('day of month', 'day of week'),
      '#description' => t('Widget changes for already used field will delete all content.'),
    );

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $options = array('day of month', 'day of week');
    $summary[] = t('Day format: @format', array('@format' => $options[$this->getSetting('day_format')]));

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [];
    $date_helper = new DateHelper();

    switch ($this->getSetting('day_format')) {
      case 0:
        $element['day'] = $element + array(
          '#type' => 'select',
          '#title' => t('Select day'),
          '#options' => $date_helper->days(),
          '#default_value' => $items[$delta]->day,
        );
        break;

      case 1:
        // Get weekdays ordered by system settings, plus add empty option.
        $weekdays = $date_helper->weekDays(TRUE);
        $weekdays = ['' => ''] + $date_helper->weekDaysOrdered($weekdays);

        $element['day'] = $element + array(
          '#type' => 'select',
          '#title' => t('Select day'),
          '#options' => $weekdays,
          '#default_value' => ($items[$delta]->day < 6) ? $items[$delta]->day : 0,
        );
        break;
    }
    $element['start_time'] = array(
      '#type' => 'datelistformat',
      '#title' => t('Select start time'),
      '#date_increment' => 15,
      '#date_part_order' => array("hour", "minute"),
      '#default_value' => new DrupalDateTime($items[$delta]->start_time),
      '#date_timezone' => "Europe/Paris",
      '#required' => FALSE,
    );
    $element['end_time'] = array(
      '#type' => 'datelistformat',
      '#title' => t('Select end time'),
      '#date_increment' => 15,
      '#date_part_order' => array("hour", "minute"),
      '#default_value' => new DrupalDateTime($items[$delta]->end_time),
      '#date_timezone' => "Europe/Paris",
      '#required' => FALSE,
      '#input' => TRUE,
    );

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // The widget form element type has transformed the value to a
    // DrupalDateTime object at this point. We need to convert it back to the
    // storage timezone and format.
    foreach ($values as &$item) {
      if (!empty($item['start_time']) && $item['start_time'] instanceof DrupalDateTime) {
        $date = $item['start_time'];
        $format = DATETIME_DATETIME_STORAGE_FORMAT;
        // Adjust the date for storage.
        $date->setTimezone(new \DateTimezone(DATETIME_STORAGE_TIMEZONE));
        $item['start_time'] = $date->format($format);
      }
      if (!empty($item['end_time']) && $item['end_time'] instanceof DrupalDateTime) {
        $date = $item['end_time'];
        $format = DATETIME_DATETIME_STORAGE_FORMAT;
        // Adjust the date for storage.
        $date->setTimezone(new \DateTimezone(DATETIME_STORAGE_TIMEZONE));
        $item['end_time'] = $date->format($format);
      }
    }
    return $values;
  }
}
