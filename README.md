# README #

This is Drupal 8 module that creates new field type with widget and formatter.
Field allows to set time interval during the day. It is useful to organize meeting or for day planning.  

![date interval field](http://image.prntscr.com/image/96cc566e4cf84312ba3787b843a4f8b9.png)

### How do I get set up? ###

* To set up functionality only install module and add field to any entity.
* Field depends only from fields and date modules.
* Configuration allow set day selection ether day of the week or day of current month.

### Contribution guidelines ###

* You are welcome to add some additional functionality or fix bugs. Just create new branch with small comments review of changes and I will merge it ot master branch.
